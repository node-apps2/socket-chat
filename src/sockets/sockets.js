
const { io } = require('../app')
const { Usuarios } = require('../classes/Usuarios')
const usuarios = new Usuarios();
const { crearMensaje } = require('../utils/utils')


io.on('connection', (client) => {


    client.on('entrarChat', (data, callback) => {
        if (!data.nombre || !data.sala) {
            return callback({
                error: true,
                mensaje: 'El nombre/sala es necesario'
            });
        }

        client.join(data.sala)
        let personas = usuarios.agregarPersona(client.id, data.nombre, data.sala);
        client.broadcast.to(data.sala).emit('listaPersona', usuarios.getPersonasXsala(data.sala))

        callback(usuarios.getPersonasXsala(data.sala));
    })

    client.on('disconnect', () => {
        let personaBorrada = usuarios.borrarPersonas(client.id)
        client.broadcast.to(personaBorrada.sala).emit('crearMensaje', crearMensaje('Adminitrador', `${personaBorrada.nombre} salio del chat`))
        client.broadcast.to(personaBorrada.sala).emit('listaPersona', usuarios.getPersonasXsala(personaBorrada.sala))

    })
    client.on('crearMensaje', (data) => {
        let persona = usuarios.getPersona(client.id);
        let mensaje = crearMensaje(persona.nombre, data.mensaje)
        client.broadcast.to(persona.sala).emit('crearMensaje', mensaje)

    })
    client.on('mensajePrivado', (data) => {
        let persona = usuarios.getPersona(client.id);
        let mensaje = crearMensaje(persona.nombre, data.mensaje)
        client.broadcast.to(data.para).emit('mensajePrivado', mensaje)

    })

});