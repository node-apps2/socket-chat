var socket = io();

var params = new URLSearchParams(window.location.search);

if (!params.has('nombre') || !params.has('sala')) {
    window.location = 'index.html'
    throw new Error('El nombre y la sala son necesarios')
}

var usuario = {
    nombre: params.get('nombre'),
    sala: params.get('sala')
}


socket.on('connect', function () {
    console.log('conectado al servidor');
    socket.emit('entrarChat', usuario, function (resp) {
        console.log(resp);
    })
});

socket.on('crearMensaje', function (data) {
    console.log(data);
})


socket.on('listaPersona', function (personas) {
    console.log(personas);
})

socket.on('mensajePrivado', function (mensaje) {
    console.log(mensaje);
})